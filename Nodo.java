
package grafosfinal;

import java.util.*;

public class Nodo {

	private String Nombre;
	private Collection<Arco> Adyacente;

	public void ListarAdyacente(String Grafo[], String Adyacencia[][], String Origen, int TamañoVector) {
		for(int i=0; i<TamañoVector; i++){
                     if(Grafo[i].equals(Origen)){ // busca la posición del nodo origen
                         for(int j=0; j<TamañoVector; j++){
                            if (Adyacencia[i][j].equals("#")){
                                // busca en la matriz los arcos del nodo
                                // # no tiene arco
                            }else{
                                // al tener algo que no es # 
                                // lista los nodos adyacentes
                            String nodo= Grafo[i];
                            String Adyacencias = Adyacencia[i][j];
                            System.out.printf("el nodo ",nodo," está conectado con: ", Adyacencias);
                            }
                         }   
                     }
                }
	}

	public boolean IrAdyacente(String Grafo[], String Adyacencia[][], String Origen,String Destino,int TamañoVector) {
            for(int i=0; i<TamañoVector; i++){
            //si encuentra el origen buscará el destino y mantiene en i la posicion donde está el origen para posicionarse en la matriz
                if(Grafo[i].equals(Origen)){ 
                    //busca en el vector la posision del destino
                    for(int j=0; j<TamañoVector; j++){// 
                       // busca en la matriz si existe el destino
                        if(Adyacencia[i][j].equals("#")){
                              System.out.print("No tiene conexion");
                            return false; 
                        }else{
                          return true; 
                        }
                    }
                }else{
                    System.out.print("No se encuentra el Origen");
                }
            }	
            return false;
        }

	public void Renombrar(String Grafo[],String Origen, int TamañoVector, String NuevoNombre) {
		for(int i=0; i<TamañoVector; i++){
                     if(Grafo[i].equals(Origen)){
                         Grafo[i]=NuevoNombre;
                         // busca en el vector el nombre del nodo y luego lo actualiza
                     }
                }
	}

	public void ModificarAdyacente(String Grafo[], String Adyacencia[][], String Origen, String Destino, int TamañoVector, String NuevoAdyacente) {
           Boolean resultado =  IrAdyacente(Grafo,Adyacencia,Origen,Destino,TamañoVector);	
            if (resultado = true){ // busca si existe una adyacencia en la matriz
                //Si existe elimina y luego le agrega el nuevo nodo adyacente
                EliminarAdyacente(Grafo,Adyacencia,Origen,Destino,TamañoVector);
                AgregarAdyacente(Grafo,Adyacencia,Origen,Destino,TamañoVector,NuevoAdyacente);
            }
	}

	public void AgregarAdyacente(String Grafo[], String Adyacencia[][], String Origen, String Destino, int TamañoVector,String Peso) {
		for(int i=0; i<TamañoVector; i++){
            //si encuentra el origen buscará el destino y mantiene en i la posicion donde esta el origen
                if(Grafo[i].equals(Origen)){ 
                    //busca en el vector la posision del destino y lo mantiene en j
                    for(int j=0; j<TamañoVector; j++){
                        //si encuentra el destino actualizará
                        if(Grafo[j].equals(Destino)){
                            Adyacencia[i][j]=Peso;
                         }else{
                            System.out.print("No se encuentra el Destino");
                        }
                    }
                }else{
                    System.out.print("No se encuentra el Origen");
                }
            }	
	}

	public void EliminarAdyacente(String Grafo[], String Adyacencia[][], String Origen, String Destino, int TamañoVector) {
		for(int i=0; i<TamañoVector; i++){
            //si encuentra el origen buscará el destino y mantiene en i la posicion dodnde esta el origen
                if(Grafo.equals(Origen)){
                    //busca en el vector la posición del destino
                    for(int j=0; j<TamañoVector; j++){ //recorre la matriz de adyacencias
                        //si encuentra el destino eliminará el arco modificando su valor a # 
                        if(Grafo.equals(Destino)){
                            Adyacencia[i][j]="#";
                         }else{
                             System.out.print("No se encuentra el Destino");
                        }
                    }
                }else{
                    System.out.print("No se encuentra el Origen");
                }
            }	
	}
        
        
}
package grafosfinal;
import java.io.*;

public class GrafosFinal {
    public static void main(String[] args) {
        
          InputStreamReader isr = new InputStreamReader(System.in);
          BufferedReader br = new BufferedReader (isr);
          
        try
        {
            int opcion=0;
            System.out.println("************** ************** ********");
            System.out.println("****** **************** **************");
            System.out.println("**   Opción 1: Agregar un Nodo      **");
            System.out.println("**   Opción 2: Eliminar un Nodo     **");
            System.out.println("**   Opción 3: Actualizar Peso      **");
            System.out.println("**   Opción 4: Listar Adyacente     **");
            System.out.println("**   Opción 5: Ir Adyacente         **");
            System.out.println("**   Opción 6: Renombrar Nodo       **");
            System.out.println("**   Opción 7: Modificar Adyacente  **");
            System.out.println("**   Opción 8: Agregar Adyacente    **");
            System.out.println("**   Opción 9: Eliminar Adyacente   **");
            System.out.println("**   Opción 10: Acerca de           **");
            System.out.println("**   Opción 11: Salir               **");
            System.out.println("**** ***************** ***************");
              System.out.print("************* *************** ********");
            opcion = Integer.parseInt(br.readLine());
     
            Grafo g = new Grafo();
            Arco a = new Arco();
            Nodo n = new Nodo();
             
       //Agregar Nodo
        if (opcion == 1){
            System.out.print("Ingrese Nombre del Nodo: ");
            String Nom = br.readLine();
            g.AgregarNodo(Nom,g.Grafo, g.n);
           
        }
        //Eliminar Nodo
        if (opcion == 2){
            System.out.print("Ingrese Nombre del Nodo: ");
            String Nom = br.readLine();
            g.EliminarNodo(Nom,g.Grafo, g.n); 
        }
        //Actualizar Peso
        if (opcion == 3){
            System.out.println("Ingrese Nodo Origen: ");
            String Origen = br.readLine();
            System.out.println("Ingrese Nodo Destino: ");
            String Destino = br.readLine();
            System.out.println("Ingrese Nuevo Peso: ");
            String NuevoPeso = br.readLine();
            a.ActualizarPeso(g.Grafo,g.Adyacencia,Origen,Destino, NuevoPeso,g.tamañovector);
        }  
        //Listar Adyacente
        if (opcion == 4){
            System.out.println("Ingrese Nodo Origen: ");
            String Origen = br.readLine();
            n.ListarAdyacente(g.Grafo, g.Adyacencia, Origen, g.tamañovector);
        }
        //ir Adyacente
        if (opcion == 5){
            System.out.println("Ingrese Nodo Origen: ");
            String Origen = br.readLine();
            System.out.println("Ingrese Nodo Destino: ");
            String Destino = br.readLine();
            n.IrAdyacente(g.Grafo,g.Adyacencia, Origen, Destino,g.tamañovector);
        }
        //Renombrar Nodo
        if (opcion == 6){
            System.out.println("Ingrese Nodo a renombrar: ");
            String Origen = br.readLine();
            System.out.println("Ingrese Nuevo Nombre: ");
            String NuevoNombre = br.readLine();
            n.Renombrar(g.Grafo, Origen, g.tamañovector, NuevoNombre);
        }
        //Modificar Adyacente
        if (opcion == 7){
            System.out.println("Ingrese Nodo Origen: ");
            String Origen = br.readLine();
            System.out.println("Ingrese Nodo Destino: ");
            String Destino = br.readLine();  
            System.out.println("Nuevo Nodo de Destino: ");
            String NuevoDestino = br.readLine();  
            n.ModificarAdyacente(g.Grafo,g.Adyacencia,Origen,Destino,g.tamañovector,NuevoDestino);              
        }
        //Agregar Adyacente
        if (opcion == 8){
            System.out.println("Ingrese Nodo Origen: ");
            String Origen = br.readLine();
            System.out.println("Ingrese Nodo Destino: ");
            String Destino = br.readLine();
            System.out.println("Ingrese Nodo Peso: ");
            String Peso = br.readLine(); 
            n.AgregarAdyacente(g.Grafo,g.Adyacencia,Origen,Destino,g.tamañovector,Peso);
        }
        //Elimnar Adyacente
        if (opcion == 9){
            System.out.println("Ingrese Nodo Origen: ");
            String Origen = br.readLine();
            System.out.println("Ingrese Nodo Destino: ");
            String Destino = br.readLine();
            n.EliminarAdyacente(g.Grafo, g.Adyacencia, Origen, Destino,g.tamañovector);
        }
        if (opcion == 10){
            System.out.println("Creado por: ");
            System.out.println("Rodrigo González RGR y Axel Lanas ");   
        }
        if (opcion ==11){
            System.out.println("Adios");
            System.exit(0); 
        }
        }
        catch (Exception e)
        {
           
        }
    }
}